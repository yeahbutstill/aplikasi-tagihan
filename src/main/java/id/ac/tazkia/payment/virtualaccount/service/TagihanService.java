package id.ac.tazkia.payment.virtualaccount.service;

import id.ac.tazkia.payment.virtualaccount.dao.*;
import id.ac.tazkia.payment.virtualaccount.dto.TagihanResponse;
import id.ac.tazkia.payment.virtualaccount.entity.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@Service @Transactional @Slf4j
public class TagihanService {
    private static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd");
    private static final String TIMEZONE = "GMT+07:00";

    @Autowired private RunningNumberService runningNumberService;
    @Autowired private TagihanDao tagihanDao;
    @Autowired private VirtualAccountDao virtualAccountDao;
    @Autowired private PeriksaStatusTagihanDao periksaStatusTagihanDao;
    @Autowired private BulkDeleteTagihanDao bulkDeleteTagihanDao;
    @Autowired private DebiturDao debiturDao;
    @Autowired private KafkaSenderService kafkaSenderService;
    @Autowired private ResendTagihanDao resendTagihanDao;
    @Autowired private ResendPembayaranDao resendPembayaranDao;

    public void saveTagihan(Tagihan t) {
        t.setNilaiTagihan(t.getNilaiTagihan().setScale(0, RoundingMode.DOWN));

        // tagihan baru
        if (t.getId() == null) {
            generateNomorTagihan(t);
            tagihanDao.save(t);
            for (Bank b : t.getJenisTagihan().getDaftarBank()) {
                VirtualAccount va = new VirtualAccount();
                va.setBank(b);
                va.setTagihan(t);
                virtualAccountDao.save(va);
            }
        } else {
            for (VirtualAccount va : virtualAccountDao.findByTagihan(t)) {
                va.setVaStatus(StatusTagihan.AKTIF.equals(t.getStatusTagihan()) ? VaStatus.UPDATE : VaStatus.DELETE);
                virtualAccountDao.save(va);
            }
            tagihanDao.save(t);
        }

        if(StatusTagihan.AKTIF.equals(t.getStatusTagihan())) {
            TagihanResponse response = createTagihanResponse(t);
            kafkaSenderService.sendTagihanResponse(response);
        }
    }

    public void gantiTagihan(Tagihan tagihanLama, Tagihan tagihanBaru) {
        tagihanLama.setStatusTagihan(StatusTagihan.NONAKTIF);
        tagihanDao.save(tagihanLama);

        generateNomorTagihan(tagihanBaru);
        tagihanDao.save(tagihanBaru);
        for (VirtualAccount va : virtualAccountDao.findByTagihan(tagihanLama)) {
            va.setTagihan(tagihanBaru);
            va.setVaStatus(VaStatus.UPDATE);
            virtualAccountDao.save(va);
        }

        TagihanResponse response = createTagihanResponse(tagihanBaru);
        kafkaSenderService.sendTagihanResponse(response);
    }

    private TagihanResponse createTagihanResponse(Tagihan t) {
        TagihanResponse response = new TagihanResponse();
        response.setDebitur(t.getDebitur().getNomorDebitur());
        response.setJenisTagihan(t.getJenisTagihan().getId());
        response.setKodeBiaya(t.getKodeBiaya().getId());
        response.setKeterangan(t.getKeterangan());
        response.setNilaiTagihan(t.getNilaiTagihan());
        response.setSukses(true);
        response.setNomorTagihan(t.getNomor());
        response.setTanggalTagihan(t.getTanggalTagihan());
        response.setTanggalJatuhTempo(t.getTanggalJatuhTempo());
        return response;
    }

    public void periksaStatus(Tagihan tagihan) {
        for (VirtualAccount va : virtualAccountDao.findByTagihan(tagihan)) {
            PeriksaStatusTagihan p = new PeriksaStatusTagihan();
            p.setVirtualAccount(va);
            p.setWaktuPeriksa(LocalDateTime.now());
            p.setStatusPemeriksaanTagihan(StatusPemeriksaanTagihan.BARU);
            periksaStatusTagihanDao.save(p);

            va.setVaStatus(VaStatus.INQUIRY);
            virtualAccountDao.save(va);
        }
    }

    public void bulkDeleteTagihan() {
        log.info("Memproses bulk delete tagihan");
        AtomicReference<Integer> jumlah = new AtomicReference<>(0);
        bulkDeleteTagihanDao.findAll()
                .forEach(b -> {
                    if (StatusDeleteTagihan.BELUM_DIPROSES.equals(b.getStatusDeleteTagihan())) {
                        log.info("Menghapus tagihan {} untuk debitur {}", b.getJenisTagihan().getNama(), b.getNomorDebitur());
                        Debitur d = debiturDao.findByNomorDebitur(b.getNomorDebitur());
                        if (d != null) {
                            List<Tagihan> daftarTagihan =
                                    tagihanDao.findByDebiturAndJenisTagihanAndStatusTagihan(d, b.getJenisTagihan(), StatusTagihan.AKTIF);
                            log.info("Menonaktifkan {} tagihan untuk debitur {} dan jenis tagihan {}",
                                    daftarTagihan.size(),
                                    d.getNama(), b.getJenisTagihan().getNama());

                            daftarTagihan.forEach(tagihan -> {
                                jumlah.getAndSet(jumlah.get() + 1);
                                tagihan.setStatusTagihan(StatusTagihan.NONAKTIF);
                                for (VirtualAccount va : virtualAccountDao.findByTagihan(tagihan)) {
                                    va.setVaStatus(VaStatus.DELETE);
                                    virtualAccountDao.save(va);
                                }
                                tagihanDao.save(tagihan);
                            });

                            b.setStatusDeleteTagihan(StatusDeleteTagihan.SUKSES);
                            bulkDeleteTagihanDao.save(b);
                        } else {
                            b.setStatusDeleteTagihan(StatusDeleteTagihan.ERROR);
                            bulkDeleteTagihanDao.save(b);
                        }
                    }
                });

        log.info("{} tagihan berhasil dinonaktifkan", jumlah.get());
    }

    public void resendTagihan() {
        log.info("{} tagihan akan dikirim ulang", resendTagihanDao.count());
        Iterable<ResendTagihan> daftarResend = resendTagihanDao.findAll();
        Integer processed = 0;
        for (ResendTagihan r : daftarResend) {
            Tagihan p = r.getTagihan();
            log.info("Resend tagihan {} | {} | {}",
                    p.getNomor(),
                    p.getDebitur().getNama(),
                    p.getNilaiTagihan());

            TagihanResponse response = createTagihanResponse(p);
            kafkaSenderService.sendTagihanResponse(response);
            processed = processed + 1;
            resendTagihanDao.delete(r);
        }
        log.info("{} pembayaran telah selesai dikirim ulang", processed);
    }

    public void resendPembayaran() {
        log.info("{} pembayaran akan dikirim ulang", resendPembayaranDao.count());
        Iterable<ResendPembayaran> daftarResend = resendPembayaranDao.findAll();

        Integer processed = 0;
        for (ResendPembayaran r : daftarResend) {
            Pembayaran p = r.getPembayaran();
            log.info("Resend pembayaran untuk tagihan {} | {} | {}",
                    p.getTagihan().getNomor(),
                    p.getTagihan().getDebitur().getNama(),
                    p.getJumlah());
            kafkaSenderService.sendPembayaranTagihan(p);
            processed = processed + 1;
            resendPembayaranDao.delete(r);
        }
        log.info("{} pembayaran telah selesai dikirim ulang", processed);
    }

    public void hapusTagihan(Tagihan tagihan) {
        tagihan.setStatusTagihan(StatusTagihan.NONAKTIF);
        saveTagihan(tagihan);
    }

    private void generateNomorTagihan(Tagihan t) {
        String datePrefix = DATE_FORMAT.format(LocalDateTime.now(ZoneId.of(TIMEZONE)));
        Long runningNumber = runningNumberService.getNumber(datePrefix);
        String nomorTagihan = datePrefix + t.getJenisTagihan().getKode() + String.format("%06d", runningNumber);
        t.setNomor(nomorTagihan);
    }
}
