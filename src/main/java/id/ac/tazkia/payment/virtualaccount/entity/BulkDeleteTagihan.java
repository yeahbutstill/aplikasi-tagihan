package id.ac.tazkia.payment.virtualaccount.entity;

import lombok.Data;

import javax.persistence.*;

@Entity @Data
public class BulkDeleteTagihan {

    @Id
    private String id;
    private String nomorDebitur;

    @ManyToOne @JoinColumn(name = "id_jenis_tagihan")
    private JenisTagihan jenisTagihan;

    @Column(name = "delete_status")
    @Enumerated(EnumType.STRING)
    private StatusDeleteTagihan statusDeleteTagihan;
}
