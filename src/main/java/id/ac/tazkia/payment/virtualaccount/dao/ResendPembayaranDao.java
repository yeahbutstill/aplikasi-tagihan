package id.ac.tazkia.payment.virtualaccount.dao;

import id.ac.tazkia.payment.virtualaccount.entity.ResendPembayaran;
import org.springframework.data.repository.CrudRepository;

public interface ResendPembayaranDao extends CrudRepository<ResendPembayaran, String> {
}
