package id.ac.tazkia.payment.virtualaccount.entity;

import lombok.Data;

import javax.persistence.*;

@Entity @Data
public class ResendTagihan {
    @Id @Column(name = "id_tagihan")
    private String id;

    @OneToOne @MapsId @JoinColumn(name = "id_tagihan")
    private Tagihan tagihan;
}
