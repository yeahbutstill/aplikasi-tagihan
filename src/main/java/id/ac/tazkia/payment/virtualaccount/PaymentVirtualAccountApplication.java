package id.ac.tazkia.payment.virtualaccount;

import id.ac.tazkia.payment.virtualaccount.service.TagihanService;
import lombok.extern.slf4j.Slf4j;
import nz.net.ultraq.thymeleaf.LayoutDialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.thymeleaf.dialect.springdata.SpringDataDialect;

@SpringBootApplication
@EnableScheduling
@EnableKafka
@Slf4j
public class PaymentVirtualAccountApplication implements CommandLineRunner {

    @Autowired
    private TagihanService tagihanService;

    @Override
    public void run(String... args) throws Exception {
        tagihanService.bulkDeleteTagihan();
        tagihanService.resendTagihan();
        tagihanService.resendPembayaran();
    }

    public static void main(String[] args) {
        SpringApplication.run(PaymentVirtualAccountApplication.class, args);
    }

    @Bean
    public SpringDataDialect springDataDialect() {
        return new SpringDataDialect();
    }

    @Bean
    public LayoutDialect layoutDialect() {
        return new LayoutDialect();
    }
}
